from socket import *

serverIP = "127.0.0.1"
serverPort = 10002
serverSocket = socket(AF_INET, SOCK_DGRAM)
serverSocket.bind((serverIP, serverPort))

# QTYPE: "A" = 1
# QCLASS: "IN" = 1
dns_records = {1: {1: {
    "com": {
        "google": {
            "TTL": 260,
            "Value": [
                [192, 165, 1, 1],
                [192, 165, 1, 10]
            ]
        },
        "youtube": {
            "TTL": 160,
            "Value": [
                [192, 165, 1, 2]
            ]
        }
    },
    "ca": {
        "uwaterloo": {
            "TTL": 160,
            "Value": [
                [192, 165, 1, 3]
            ]
        },
        "amazon": {
            "TTL": 160,
            "Value": [
                [192, 165, 1, 5]
            ]
        }
    },
    "org": {
        "wikipedia": {
            "TTL": 160,
            "Value": [
                [192, 165, 1, 4]
            ]
        }
    }
}}}

'''
    DNS Format
    
     0  1  2  3  4  5  6  7  8  9  10 11 12 13 14 15
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  0 |                     ID                        |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  2 |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  4 |                   QDCOUNT                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  6 |                   ANCOUNT                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
  8 |                   NSCOUNT                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 10 |                   ARCOUNT                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /               question sections               /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                answer sections                /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    (authoritative and additional records sections omitted)
    
    For this lab, the client will send:
    ID = random
    QR = 0
    OPCODE = 0000
    AA = 1
    TC = 0
    RD = 0
    RA = 0
    Z = 000
    RCODE = 0000
    QDCOUNT = 1
    ANCOUNT = 0
    NSCOUNT = 0
    ARCOUNT = 0
    
    The server will copy all values as is, except:
    QR = 1
    ANCOUNT = number of matching records
    
    Question section
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                    QNAME                      /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QTYPE                      |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QCLASS                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    
    For this lab, the client will send:
    QNAME = varies
    QTYPE = 0x00 01
    QCLASS = 0x00 01
    
    The server will copy all values as is.
    
    Answer section
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                    NAME                       /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    TYPE                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    CLASS                      |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     TTL                       |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                  RDLENGTH                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    /                    RDATA                      /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    
    For this lab, the client will send no answer sections.
    
    The server will send (for each answer):
    NAME = 0xc0 0c
    TYPE = 0x00 01
    CLASS = 0x00 01
    TTL = as given by DNS table
    RDLENGTH = 0x00 04 (4 byte ip address)
    RDATA = ip address
'''


def decodeUint16(reqBytes, index):
    ret = 0
    ret += reqBytes[index]
    ret = ret << 8
    ret += reqBytes[index + 1]
    return ret


# Query QDCOUNT for number of question sections
def decodeQDCount(reqBytes):
    return decodeUint16(reqBytes, 4)


# Server needs to update the header in the response
def updateHeader(respBytes, qr, ancount):
    # update QR
    qr = qr & 1
    respBytes[2] = (respBytes[2] & 0x7f) | (qr << 7)

    # update ANCOUNT
    ancount = ancount & 0xffff
    respBytes[6] = (ancount >> 8) & 0xff
    respBytes[7] = ancount & 0xff


# Question section does not need to be updated by server
# However, we want the information to formulate the answers
def decodeQuestion(reqBytes, startIndex):
    # decode QNAME
    index = startIndex
    qname = []
    while True:
        length = reqBytes[index]
        index += 1
        if length == 0:
            break
        name = ""
        for i in range(length):
            name += chr(reqBytes[index])
            index += 1
        qname.append(name)

    # QTYPE and QCLASS
    qtype = decodeUint16(reqBytes, index)
    index += 2
    qclass = decodeUint16(reqBytes, index)
    index += 2

    return qname, qtype, qclass, index


# Answer section
#   TTL given as a 32 bit unsigned integer
#   IP address given as a byte array of 4 bytes
def encodeAnswer(q_index, q_type, q_class, ttl, ip_addr):
    # NAME, TYPE, CLASS
    name = 0xc000  # pointer
    name = name | q_index
    ansBytes = list(name.to_bytes(2, 'big'))
    ansBytes += list(q_type.to_bytes(2, 'big'))
    ansBytes += list(q_class.to_bytes(2, 'big'))

    # TTL is 32 bits signed
    ttlBytes = list(ttl.to_bytes(4, 'big', signed=True))

    ansBytes += list(ttlBytes)
    ansBytes += list(len(ip_addr).to_bytes(2, 'big'))  # RDLENGTH should be 4
    ansBytes += ip_addr  # ip_addr should be a list of length 4

    return ansBytes


def toTwoDigitHex(k):
    ret = hex(k)[2:]
    if len(ret) == 1:
        ret = "0" + ret
    return ret


while True:
    message, clientAddr = serverSocket.recvfrom(2048)
    request = list(message)

    print('Request:')
    print(' '.join(map(toTwoDigitHex, request)))

    qr = 1
    ancount = 0
    q_count = decodeQDCount(request)
    ind = 12  # questions start at 12

    try:
        for i in range(q_count):
            q_name, q_type, q_class, nextIndex = decodeQuestion(request, ind)

            # lookup in DNS records
            lookup = dns_records[q_type][q_class]
            for part in q_name[::-1]:  # domain name is reversed
                if part not in lookup:
                    raise ValueError("Error! Domain name not found in DNS records")

                lookup = lookup[part]

            if "TTL" not in lookup or "Value" not in lookup:
                raise ValueError("Error! Invalid domain name or TTL, Value not found in DNS records")

            time_to_live = lookup["TTL"]
            for ip_address in lookup["Value"]:
                request += encodeAnswer(ind, q_type, q_class, time_to_live, ip_address)
                ancount += 1

            ind = nextIndex
    except ValueError as e:
        print(e)

    updateHeader(request, qr, ancount)

    print('Response:')
    print(' '.join(map(toTwoDigitHex, request)))

    serverSocket.sendto(bytearray(request), clientAddr)
