from socket import *
from random import random
from math import floor

ANSWER_START = 12
#get a random 16 bit id
def getId():
    return floor(random() * (2**16 - 1))

#for parsing the byte array 
def getType(byteArray):
    if int.from_bytes(byteArray[ANSWER_START + 2: ANSWER_START + 4], "big") == 1:
        return "A"
    else:
        return "!"

def getClass(byteArray):
    if int.from_bytes(byteArray[ANSWER_START + 4: ANSWER_START + 6], "big") == 1:
        return "IN"
    else:
        return "!"

def getTTL(byteArray):
    return int.from_bytes(byteArray[ANSWER_START + 6:ANSWER_START + 10], "big", signed=True)

def getRDLength(byteArray):
    return int.from_bytes(byteArray[ANSWER_START + 10:ANSWER_START + 12], "big")

def getRData(byteArray):
    int1 = byteArray[ANSWER_START + 12]
    int2 = byteArray[ANSWER_START + 13]
    int3 = byteArray[ANSWER_START + 14]
    int4 = byteArray[ANSWER_START + 15]
    return str(int1) + "." + str(int2) + "." + str(int3) + "." + str(int4)

def getANCount(byteArray):
    return int.from_bytes(byteArray[6: 8], "big")

def printLine(byteArray, domainName):
    _type = getType(byteArray)
    _class = getClass(byteArray)
    _ttl = getTTL(byteArray)
    _rdLength = getRDLength(byteArray)
    _rData = getRData(byteArray)
    print(domainName + ": type " + _type + ", class " + _class + ", TTL " + str(_ttl) + ", addr (" + str(_rdLength) + ") " + _rData)


serverIP = "127.0.0.1"
serverPort = 10002

while True:
    clientSocket = socket(AF_INET, SOCK_DGRAM)
    byte_array = bytearray([])
    #Send Header
    byte_array += getId().to_bytes(2, "big")
    # 00000100: QR --> RD
    byte_array += (4).to_bytes(1, "big")
    # 00000000: RA --> RCODE
    byte_array += (0).to_bytes(1, "big")
    # QDCOUNT
    byte_array += (1).to_bytes(2, "big")
    # ANCOUNT
    byte_array += (0).to_bytes(2, "big")
    # NSCOUNT
    byte_array += (0).to_bytes(2, "big")
    # ARCOUNT
    byte_array += (0).to_bytes(2, "big")
    #Send Question
    #QNAME
    domain_name = input("Enter Domain Name: ")
    if domain_name == "end":
        print("Session ended")
        break
    name = domain_name.split(".")[0]
    tld = domain_name.split(".")[1]
    name_bytes =  bytes(name,'ascii')
    tld_bytes = bytes(tld,'ascii')
    byte_array += ((len(name_bytes)).to_bytes(1, "big"))
    byte_array += (name_bytes)
    byte_array += ((len(tld_bytes)).to_bytes(1, "big"))
    byte_array += (tld_bytes)
    byte_array += ((0).to_bytes(1, "big"))  # null terminator
    
    #QTYPE
    byte_array += ((1).to_bytes(2, "big"))
    #QCLASS
    byte_array += ((1).to_bytes(2, "big"))

    ANSWER_START = len(byte_array)

    clientSocket.sendto(byte_array, (serverIP, serverPort))

    msg, serverAddress = clientSocket.recvfrom(2048)
    anCount = getANCount(msg)
    while True:
        printLine(msg, domain_name)
        anCount = anCount - 1
        ANSWER_START += 16  # fixed name and rdlength, so just hard code 16 bytes
        if anCount == 0:
            break
clientSocket.close()
