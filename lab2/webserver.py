from socket import *
from email.utils import formatdate
from datetime import datetime
from time import mktime
from pathlib import Path
import os

def getHttpNow():
    now = datetime.now()
    stamp = mktime(now.timetuple())
    return formatdate(stamp, False, True)

def getLastModified(filePath):
    if(os.path.exists(filePath)):
        stamp = os.path.getmtime(filePath)
        return formatdate(stamp, False, True)
    return "0"


serverIP = "127.0.0.1"
serverPort = 10002
serverSocket = socket(AF_INET,SOCK_STREAM)
serverSocket.bind((serverIP, serverPort))
serverSocket.listen(1)
print("Ready to receive")

while True:
    connectionSocket, addr = serverSocket.accept()
    sentence = connectionSocket.recv(2048).decode()

    # Message body - find file in filesystem
    reqPath = sentence.split()[1]
    print(reqPath)
    filePath = Path("." + reqPath)
    
    # Message headers
    h_connection = "Connection: keep-alive"
    h_date = "Date: " + getHttpNow()
    h_server = "Server: Lab2/1.0"
    h_lastModified = "Last-Modified: " + getLastModified(filePath)
    h_contentType = "Content-Type: text/html"
    h_contentLength = "Content-Length: "

    # File does not exist - send 404 and end early
    if not filePath.exists() or not filePath.is_file():
        connectionSocket.send('HTTP/1.0 404 Not Found\n'.encode())
        connectionSocket.send((h_connection + "\n").encode())
        connectionSocket.send((h_date + "\n").encode())
        connectionSocket.send((h_server + "\n").encode())
        connectionSocket.close()
        continue

    # Get file size
    h_contentLength += str(filePath.stat().st_size)

    # File exists, send 200 OK
    connectionSocket.send('HTTP/1.0 200 OK\n'.encode())

    # Send headers
    header = [h_connection, h_date, h_server, h_lastModified, h_contentLength, h_contentType]
    for x in header:
        connectionSocket.send((x + "\n").encode())

    # Send body if applicable (i.e. not a HEAD request)
    reqType = sentence.split()[0]
    if reqType != "HEAD":
        connectionSocket.send("\n".encode())  # add extra space for body
        with filePath.open("rb") as f:
            l = f.read(1024)
            while (l):
                connectionSocket.send(l)
                l = f.read(1024)

    # done
    connectionSocket.close()
