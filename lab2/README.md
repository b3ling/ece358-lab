# ECE358 Lab2

## Part1: webserver.py

### Instructions
1. Start the server with `python3 webserver.py`
2. Open browser with url `http://127.0.0.1:10002/<file>` and request a file

## Part2 : DNS server

### Files
- `client.py`
- `server.py`

### Instructions
1. Open a terminal and run `python3 server.py` to start the server
2. Open an additional terminal and run `python3 client.py` to start the client
