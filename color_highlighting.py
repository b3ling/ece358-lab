# colors = ['\x1b[38;2;100;200;0m', '\x1b[94m\x1b[96m', '\x1b[92m', '\x1b[93m', '\x1b[91m']
endc = '\x1b[0m'
block_sizes = [2, 2, 2, 2, 2, 2, 15, 2, 2]  # query
block_sizes = [2, 2, 2, 2, 2, 2, 15, 2, 2,
               2, 2, 2, 4, 2, 4]
               # 2, 2, 2, 4, 2, 4]

colors_gen = []

r = 255
g = 0
b = 0
count_blocks = len(block_sizes)
change = int(255 / count_blocks) * 6

for block in block_sizes:
    colors_gen.append('\x1b[38;2;' + str(r) + ';' + str(g) + ';' + str(b) + 'm')
    if r > 0 and g < 255:
        g = min(g + change, 255)
    elif r > 0 and g == 255:
        r = max(r - change, 0)
    elif g > 0 and b < 255:
        b = min(b + change, 255)
    elif g > 0 and b == 255:
        g = max(g - change, 0)
    elif b > 0 and r < 255:
        r = min(r + change, 255)
    elif b > 0 and r == 255:
        b = max(b - change, 0)

colors = []

j = 0
k = 0

for i in range(len(colors_gen)):
    colors.append(colors_gen[j])
    j += 6
    if j >= len(colors_gen):
        k += 1
        j = k

tokens = "b7 53 83 00 00 01 00 01 00 00 00 00 09 77 69 6b 69 70 65 64 69 61 03 6f 72 67 00 00 01 00 01 c0 0c 00 01 00 01 00 00 00 a0 00 04 c0 a5 01 04".split()

assert sum(block_sizes) == len(tokens)

ansi = ""

idx = 0
for blk_idx, blk_size in enumerate(block_sizes):
    for _ in range(blk_size):
        # print(f"{colors[blk_idx % len(colors)]}{tokens[idx]}{endc}", end=" ")
        ansi += f"{colors[blk_idx % len(colors)]}{tokens[idx]}{endc} "
        idx += 1


from ansi2html import Ansi2HTMLConverter

conv = Ansi2HTMLConverter()
html = conv.convert(ansi)
with open('test.html', 'w') as f:
    f.write(html)
