from math import log
from statistics import mean, variance
from random import random

n = 1000
lamb = 75
values = []
for i in range(n):
    values.append((-1/lamb) * log(1-random()))

print("Mean: {:.6f}".format(mean(values)))
print("Variance: {:.8f}".format(variance(values)))

