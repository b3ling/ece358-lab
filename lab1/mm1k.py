from common import plot
from common import sim

#constants
T = 3000
L = 2000
C = 10**6 
rhos = [0.5, 0.6, 0.7, 0.8, 0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5]
Ens = [] # float[]
PLosses = [] # float[]
ks = [10, 25, 50]

print("Starting simmulations for T ===========")
for k in ks:
    EnsForK = []
    PLossesForK = []
    for rho in rhos:
        results = sim(k, rho, T, L, C)
        EnsForK.append(results['En'])
        PLossesForK.append(results['Ploss'])
        print("Finished simulation for k =", k, "rho =", rho)
    Ens.append(EnsForK)
    PLosses.append(PLossesForK)

T = 4000
Ens2 = [] # float[]
PLosses2 = [] # float[]

print("Starting simmulations for T ===========")
for k in ks:
    EnsForK = []
    PLossesForK = []
    for rho in rhos:
        results = sim(k, rho, T, L, C)
        EnsForK.append(results['En'])
        PLossesForK.append(results['Ploss'])
        print("Finished simulation for k =", k, "rho =", rho)
    Ens2.append(EnsForK)
    PLosses2.append(PLossesForK)

Errors = []
for i, en in enumerate(Ens):
    errors=[]
    for j, e in enumerate(en):
        e2 = Ens2[i][j]
        errors.append((abs(e-e2) / e) * 100)
    Errors.append(errors)

folderName = "graphs"
xLabel = "Traffic Density (rho)"
yLabelEn = "Average number of packets in queue (E[N])"
yLabelPLoss = "Packet loss probability (P loss)"
plot(rhos, Ens, "E[n] vs. rho for mm1k", xLabel, yLabelEn,"./"+folderName+"/Ens-mm1k.png", ["k=10", "k=25", "k=50"] )
plot(rhos, PLosses, "P loss vs. rho for mm1k", xLabel, yLabelPLoss,"./"+folderName+"/Ploss-mm1k.png", ["k=10", "k=25", "k=50"] )

folderName = "graphs2"
xLabel = "Traffic Density (rho)"
yLabelEn = "Average number of packets in queue (E[N])"
yLabelPLoss = "Packet loss probability (P loss)"
plot(rhos, Ens2, "E[n] vs. rho for mm1k", xLabel, yLabelEn,"./"+folderName+"/Ens-mm1k.png", ["k=10", "k=25", "k=50"] )
plot(rhos, PLosses2, "P loss vs. rho for mm1k", xLabel, yLabelPLoss,"./"+folderName+"/Ploss-mm1k.png", ["k=10", "k=25", "k=50"] )

folderName = "errorGraphs"
xLabel = "Traffic Density(rho)"
plot(rhos, Errors, "Error for E[n] by traffic density (rho) for mm1k", xLabel, "error percentage (%)","./"+folderName+"/Ens-mm1k-error.png", ["k=10", "k=25", "k=50"] )
