from common import plot
from common import sim
 
rhos = [0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.95, 1.2]
events = []
T = 3000
L = 2000
C = 10**6

Ens = []
PIdles = []
k = float("inf")
for rho in rhos:
    results = sim(k, rho, T, L, C)
    Ens.append(results['En'])
    PIdles.append(results['Pidle'])
    print("Finished simulation for k =", k, "rho =", rho)

Ens2 = []
PIdles2 = []
T = 4000
for rho in rhos:
    results = sim(k, rho, T, L, C)
    Ens2.append(results['En'])
    PIdles2.append(results['Pidle'])
    print("Finished simulation for k =", k, "rho =", rho)

xLabel = "Traffic Density (rho)"
yLabelEn = "Average number of packets in queue (E[N])"
yLabelIdle = "Proportion of time the system is idle (P idle)"
# Graph T
#use [:-1] to not incude the rho = 1.2 value
folderName = "graphs"
print("Generating Graphs for T ==============")
plot(rhos[:-1], [Ens[:-1]], "E[n] vs. rho", xLabel, yLabelEn,"./"+folderName+"/Ens-mm1.png" )
plot(rhos, [Ens], "E[n] vs. rho (with rho=1.2)", xLabel, yLabelEn,"./"+folderName+"/Ens-mm1-1.2.png" )
plot(rhos[:-1], [PIdles[:-1]], "P idle vs. rho", xLabel, yLabelIdle, "./"+folderName+"/Pidle-mm1.png")
plot(rhos, [PIdles], "P idle vs. rho (with rho=1.2)",  xLabel, yLabelIdle, "./"+folderName+"/Pidle-mm1-1.2.png")
# Graph 2T
folderName = "graphs2"
print("Generating Graphs for 2T ==============")
plot(rhos[:-1], [Ens2[:-1]], "E[n] vs. rho", xLabel, yLabelEn,"./"+folderName+"/Ens-mm1.png" )
plot(rhos, [Ens2], "E[n] vs. rho (with rho=1.2)", xLabel, yLabelEn,"./"+folderName+"/Ens-mm1-1.2.png" )
plot(rhos[:-1], [PIdles2[:-1]], "P idle vs. rho", xLabel, yLabelIdle, "./"+folderName+"/Pidle-mm1.png")
plot(rhos, [PIdles2], "P idle vs. rho (with rho=1.2)",  xLabel, yLabelIdle, "./"+folderName+"/Pidle-mm1-1.2.png")
# graph error
EnsError = []
PidleError = []
folderName = "errorGraphs"
for i, en in enumerate(Ens[:-1]):
    en2 = Ens2[i]
    EnsError.append((abs(en-en2) / en) * 100)
for i, pidle in enumerate(PIdles[:-1]):
    pidle2 = PIdles2[i]
    PidleError.append((abs(pidle2-pidle) / pidle)*100)
folderName = "errorGraphs"
print("Generating Error Graphs ==============")
plot(rhos[:-1], [EnsError], "Error for E[n] by traffic density (rho)", xLabel, "Error percentage %", "./"+folderName+"/Ens-mm1-error.png" )
plot(rhos[:-1], [PidleError], "Error for P idle by traffic density (rho)", xLabel, "Error percentage %", "./"+folderName+"/Pidle-mm1-error.png" )
