from random import random
from matplotlib import pyplot
from matplotlib import style
from math import log
from statistics import mean

class EventType:
    ARRIVAL = 1
    DEPARTURE = 2
    OBSERVATION = 3

# exponential distribution
def rand(lmda):
    return (-1/lmda) * log(1-random())

class Event:
    def __init__(self, eventType, time):
        self.eventType = eventType
        self.time = time

#Output: Events[]
#List of events sorted by time with type ARRIVAL and OBSERVATION
def genArrivalAndObserverEvents(rho, T, L, C):
    print("Starting arrival and observation events for rho", rho, "==============")
    lmbda = rho * (C / L)
    arrivals = [Event(EventType.ARRIVAL, 0)] #init with time 0 just as helper
    observers = [Event(EventType.OBSERVATION, 0)] #init with time 0 just as helper
    while(arrivals[-1].time < T):
        arrivals.append(Event(EventType.ARRIVAL, arrivals[-1].time + rand(lmbda)))
    while(observers[-1].time < T):
        observers.append(Event(EventType.OBSERVATION, observers[-1].time + rand(lmbda * 5)))
    arrivals.pop(0) #remove the helper init value
    observers.pop(0) #remove the helper init value
    events = arrivals + observers
    events.sort(key=lambda event: event.time)
    print("Done arrival and observation events for rho", rho)
    return events

# x: float[] common x values of all y arrays
# ys: float[][] array of array of y values to plot based on x values
#       to plot multiple lines for mm1k
# legendVals: Legend strings map to lines from ys in order
def plot(x, ys, title, xLabel, yLabel, outDir, legendVals=[]):
    style.use("ggplot")
    style.use("dark_background")
    for y in ys:
        pyplot.plot(x, y, linestyle='--', marker='o')
    if legendVals:
        pyplot.legend(legendVals, loc="best")
    pyplot.title(title)
    pyplot.xlabel(xLabel)
    pyplot.ylabel(yLabel)
    pyplot.savefig(outDir)
    pyplot.clf()

#queue: Event[]
#return: # of elements that left the queue
def updateQueue(queue, currentTime):
    count = 0
    while len(queue) > 0 and queue[0].time < currentTime:
        queue.pop(0)
        count += 1
    return count
    
# return:
# {
#   'Ens' : float[] <-- list of average number of packets in queue for each rho value
#   'PIdle' : float[] <-- list of times empty / observations for each rho value
#   'Ploss' : float[] <-- number of packets lost by caluclation below
# }
# idea: 
# always count arrivals, if it didn't depart then we lost it
# so, arrivals = length of buffer + departed + lost packets
# rearrange the above expression for # of lost packets
def sim(k, rho, T, L, C):
    #init counters
    Na = 0 #arrivals
    Nd = 0 #departures
    No = 0 #observations
    Ens = []
    PLosses = []
    currentArrivalTime = 0
    queue = [] #array of Events with type Departure
    nEmptyTimes = 0

    events = genArrivalAndObserverEvents(rho, T, L, C)

    for event in events:
        if event.eventType == EventType.ARRIVAL:
            Nd += updateQueue(queue, event.time) #remove any elements out of the queue
            Na += 1
            if(len(queue) >= k): #drop the packet
                continue
            processTime = rand(1/L) / C
            if(len(queue) == 0):
                queue.append(Event(EventType.DEPARTURE, event.time + processTime))
            else:
                queue.append(Event(EventType.DEPARTURE, queue[-1].time + processTime))
        elif event.eventType == EventType.OBSERVATION: #observer type
            No += 1
            Nd += updateQueue(queue, event.time) #remove any elements out of the queue
            Ens.append(len(queue))
            if(Na > 0):
                PLosses.append((Na - len(queue) - Nd) / Na)
            else:
                PLosses.append(0)
            if len(queue) <= 0:
                nEmptyTimes += 1
        else:
            raise ValueError()
    return{
        'En': mean(Ens),
        'Ploss': mean(PLosses),
        'Pidle' : nEmptyTimes / No
    }