# Lab 1 ECE358
## Q1 results

1. run `python3 q1.py`

## Q3 results

1. run `python3 mm1.py` and wait for the script to finish
2. The folder`./graphs` will then have the generate graphs as described by the file name. This is for simulation time `n*T` where `n*T` is defined in `mm1.py`:
    - `Ens-mm1.png`
    - `Ens-mm1-1.2.png` (includes rho = 1.2)
    - `Pidle-mm1.png`
    - `Pidle-mm1-1.2.png`(includes rho = 1.2)
3. `./graphs2` folder contains the same graphs as from step 2, but with simulation time `(n+1)*T`. This is for measuring error handling purposes

4. `./errorGraphs` folder contains error percentage graphs of E[n] to ensure the chosen simulation times of `n*T` and `(n+1) * T` have an error percentage of no more than 5%:
    - `Ens.mm1.error.png`
## Q6 results

1. run `python3 mm1k.py` and wait for the script to finish
2. The folder`./graphs` will then have the generate graphs as described by the file name. This is for simulation time `n*T`. `n*T` is defined in `mm1k.py`:
    - `Ens-mm1k.png`
    - `Ploss-mm1k.png`

3. `./graphs2` folder contains the same graphs as from step 2, but with simulation time `(n+1)*T`. This is for measuring error handling purposes. `(n+1)*T` is defined in `mm1k.py`

4. `./errorGraphs` folder contains error percentage graphs of E[n] to ensure the chosen simulation times of n*T and (n+1) * T have an error percentage of no more than 5%:
    - `Ens-mm1k-error.png`
